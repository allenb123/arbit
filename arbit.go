/*
Package arbit is a minimal
event-driven, websocket library.

Message Format

Messages are formatted as 2-element JSON arrays:
[eventName, messageData].
Therefore, the message data
must be JSON-serializable.

Concurrency

All functions and methods in this package
are safe for concurrent use, with the exception
of the On and OnClose handlers. arbit handles
the concurrency problems for you.

Example: As Server

	arb := arbit.NewServer()

	arb.On("echo", func(cl *arbit.Client, data interface{}) {
		cl.Send("echo", "Echo: " + fmt.Sprint(data))
	})

	http.Handle("/ws", srv)
	http.ListenAndServe(":8080", nil)

Example: As Client

	arb := arbit.NewClient("http://localhost:8080/ws", http.Header{})

	arb.On("echo", func(data interface{}) {
		log.Println("Message: " + fmt.Sprint(data))
		arb.Close()
	})
	arb.Send("echo", "Hello world!")
	arb.Listen()

*/
package arbit

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"sync"
)

// Type Server represents a server. It manages
// a set of websocket connections.
type Server struct {
	upgrader websocket.Upgrader

	mutex   sync.RWMutex
	clients []*Client

	handlers     map[string]func(*Client, interface{})
	closeHandler func(*Client, int)
}

// Function NewServer creates a new Server and returns it.
func NewServer() *Server {
	return &Server{
		upgrader: websocket.Upgrader{ReadBufferSize: 1024, WriteBufferSize: 1024},
		handlers: make(map[string]func(cl *Client, data interface{})),
	}
}

// Method ServeHTTP satisfies the http.Handler interface.
// It will spawn a new goroutine for each websocket
// connection.
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		return
	}

	s.mutex.Lock()
	s.clients = append(s.clients, &Client{conn: conn})
	go s.listen(s.clients[len(s.clients)-1], len(s.clients)-1)
	s.mutex.Unlock()
}

// Method On registers a handler for a given event. Each
// event can have at most one handler.
//
// Calling On and OnClose on the given client will have no effect.
func (s *Server) On(event string, handler func(cl *Client, data interface{})) {
	s.handlers[event] = handler
}

// Method OnClose registers a close handler, which
// is called when a connection is closed.
// There can be at most one close handler.
//
// The code passed in is the code passed
// to Client.Close; see Client.Close for
// more information.
//
// Calling On and OnClose on the given client will have no effect.
func (s *Server) OnClose(handler func(cl *Client, code int)) {
	s.closeHandler = handler
}

func (s *Server) listen(client *Client, index int) {
	for {
		client.rmutex.Lock()
		mt, body, err := client.conn.ReadMessage()
		client.rmutex.Unlock()

		// CloseMessage
		if closeerr, ok := err.(*websocket.CloseError); ok {
			var bytes [2]byte
			binary.BigEndian.PutUint16(bytes[:], uint16(closeerr.Code))
			client.wmutex.Lock()
			client.conn.WriteMessage(websocket.CloseMessage, bytes[:])
			client.wmutex.Unlock()
			s.clients[index] = nil

			if s.closeHandler != nil {
				s.closeHandler(client, closeerr.Code)
			}

			break
		}
		// Misc Error
		if err != nil {
			client.conn.Close()
			s.clients[index] = nil
			break
		}
		// PingMessage
		if mt == websocket.PingMessage {
			client.wmutex.Lock()
			client.conn.WriteMessage(websocket.PongMessage, body)
			client.wmutex.Unlock()
			continue
		}

		var res []interface{}
		err = json.Unmarshal(body, &res)
		if len(res) == 0 {
			continue
		}

		// Normal
		if str, ok := res[0].(string); ok {
			// Run handlers
			if handler := s.handlers[str]; handler != nil {
				handler(client, res[1])
			}
		}
	}
}

// Method Clients returns a list of Clients
// that are currently connected to the server.
func (s *Server) Clients() []*Client {
	s.mutex.RLock()
	out := append([]*Client(nil), s.clients...)
	s.mutex.RUnlock()
	return out
}

// Type Client represents a
// websocket connection.
type Client struct {
	conn         *websocket.Conn
	rmutex       sync.Mutex
	wmutex       sync.Mutex
	handlers     map[string]func(interface{})
	closeHandler func(int)
}

// Function NewClient creates a new
// Client from a given url.
func NewClient(url string, header http.Header) (*Client, error) {
	dialer := websocket.Dialer{ReadBufferSize: 1024, WriteBufferSize: 1024}
	conn, _, err := dialer.Dial(url, header)
	if err != nil {
		return nil, err
	}
	return &Client{conn: conn, handlers: make(map[string]func(interface{}))}, nil
}

// Method Send sends a message through
// the connection represented by the
// Client.
func (cl *Client) Send(event string, data interface{}) error {
	cl.wmutex.Lock()
	defer cl.wmutex.Unlock()
	return cl.conn.WriteJSON([]interface{}{event, data})
}

// Method Close closes the connection
// with a given code. Codes
// 4000–4999 are available for use by applications.
//
// See https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent
// for more information about close codes.
func (cl *Client) Close(code uint16) error {
	cl.wmutex.Lock()
	defer cl.wmutex.Unlock()
	var bytes [2]byte
	binary.BigEndian.PutUint16(bytes[:], code)
	cl.conn.WriteMessage(websocket.CloseMessage, bytes[:])
	return cl.conn.Close()
}

// Method On registers an event handler.
// Each event can have at most one
// handler.
//
// The event handler will be executed in the
// goroutine that called Listen.
func (cl *Client) On(event string, handler func(data interface{})) {
	cl.handlers[event] = handler
}

// Method OnClose registers a close
// handler, to be called when the connection
// is closed. Each Client can only have
// one close handler.
//
// The close handler will be executed in the
// goroutine that called Listen.
func (cl *Client) OnClose(event string, handler func(code int)) {
	cl.closeHandler = handler
}

// Method Listen starts the event loop
// that will call the event and close handlers.
func (cl *Client) Listen() error {
	for {
		cl.rmutex.Lock()
		var res []interface{}
		err := cl.conn.ReadJSON(&res)
		cl.rmutex.Unlock()

		if closeerr, ok := err.(*websocket.CloseError); ok {
			if cl.closeHandler != nil {
				cl.closeHandler(closeerr.Code)
			}
			cl.conn.Close()
			break
		}
		if err != nil {
			return fmt.Errorf("%v", err)
		}
		if str, ok := res[0].(string); ok {
			// Run handlers
			if handler := cl.handlers[str]; handler != nil {
				handler(res[1])
			}
		}
	}
	return nil
}

// Function Broadcast sends a message
// to multiple Clients.
func Broadcast(clients []*Client, event string, data interface{}) error {
	body, err := json.Marshal([2]interface{}{event, data})
	if err != nil {
		return err
	}
	msg, err := websocket.NewPreparedMessage(websocket.TextMessage, body)
	if err != nil {
		return fmt.Errorf("failed to prepare message: %v", msg)
	}
	for _, client := range clients {
		if client == nil {
			continue
		}

		client.wmutex.Lock()
		client.conn.WritePreparedMessage(msg)
		client.wmutex.Unlock()
	}
	return nil
}
